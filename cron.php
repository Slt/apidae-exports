<?php
// Inclusion des paramètres et objets utiles.
require_once dirname(__FILE__).'/config.php';

/*
Vérifie la présence d'un fichier de notification enregistré dans le DATAPATH
Si le fichier est présent :
	- téléchargement du fichier
	- extraction des fichiers contenus dans l'archive
	- renvoi d'une confrimation à Apidae
	- suppression du fichier de notification
*/
$e = new sbcd_export(DATAPATH);
return $e->cronWalk();
?>