<?php
// Inclusion des paramètres et objets utiles.
require_once dirname(__FILE__).'/config.php';

/*
Reçoit les notifications en provenance d'Apidae et enregistre les données
dans le fichier notification.json situé dans le dossier DATAPATH
*/
$e = new sbcd_export(DATAPATH);
echo $e->getNotification();
?>