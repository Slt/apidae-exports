<?php
// Affichage des erreurs durant les développements.
//ini_set('display_errors', 1);
//error_reporting(E_ALL);
//ini_set('max_execution_time', 900);	// Temps d'exectution à 15 minutes.


// Définition des paramètres
// ====================================
// Dossier de stockage des fichiers d'export. Le dossier doit permettre l'écriture.
define('DATAPATH',dirname(__FILE__).'/data/');


// Chargement des fichiers utiles
// ====================================
require_once dirname(__FILE__).'/sbcd_apidae/sbcd_tools.php';
require_once dirname(__FILE__).'/sbcd_apidae/sbcd_export.php';

// ln18
function __($string) {
	return $string;
}
?>