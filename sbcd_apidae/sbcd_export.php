<?php
// require_once 'sbcd_tools.php';

class sbcd_export {
	public $projetId;
	public $statut;
	public $ponctuel;
	public $reinitialisation;
	public $urlRecuperation;
	public $urlConfirmation;

	public $cachePath;

	/**
	 * Constructeur. Doit être appelé en envoyant le chemin réel du dossier utilisé pour le cache.
	 *
	 * @access public
	 * @param mixed $cachePath (default: null)
	 * @return void
	 */
	public function __construct($cachePath=null) {
		if ($cachePath != null) {
			$this->cachePath = $cachePath;
			return true;
		}
		return false;
	}

	/**
	 * Récupère une notification et enregistre son contenu.
	 *
	 * @access public
	 * @static
	 * @param mixed $args
	 * @return string
	 *
	 * @TODO prévoir de recevoir des rafales de notifications et pouvoir les traiter.
	 * @TODO vérifier que le domaine de la récupération et de la confirmation est soit apidae-tourisme.com soit sitra2-vm-preprod.accelance.net
	 * @TODO Enregistrement dans un dossier spécifique et traitement par ordre chronologique.
	 */
	public function getNotification() {
		if (empty($_POST) || ($_SERVER['SERVER_NAME'] != 'localhost' && $_SERVER['SERVER_ADDR'] == '213.162.48.205')) {
			return __('Cette URL est un webService à distination d\'Apidae uniquement.');
		}
		// Catching notification values
		$this->projetId 			= $_POST['projetId'];
		$this->statut 				= $_POST['statut'];
		$this->ponctuel 			= $_POST['ponctuel'];
		$this->reinitialisation 	= $_POST['reinitialisation'];
		$this->urlRecuperation 		= $_POST['urlRecuperation'];
		$this->urlConfirmation 		= $_POST['urlConfirmation'];

		// Saving file with notification datas to be use later
		$notificationFile = $this->cachePath.'/notification.json';

		if ($this->statut=='SUCCESS'){
			sbcd_tools::writeFile ($notificationFile,json_encode($this));
			return __('Merci Apidae !');
		}

		return __('Appelez Vladimir !');
	}

	/**
	 * Recharge l'objet avec les données obtenues durant la notification d'Apidae.
	 *
	 * @access public
	 * @return void
	 */
	public function loadNotification() {
		if ($notiFile = $this->isNotificationFile()){
			$notifRaw = sbcd_tools::readFile($notiFile);
			$notif = json_decode($notifRaw);

			$this->projetId 			= $notif->projetId;
			$this->statut 				= $notif->statut;
			$this->ponctuel 			= $notif->ponctuel;
			$this->reinitialisation 	= $notif->reinitialisation;
			$this->urlRecuperation 		= $notif->urlRecuperation;
			$this->urlConfirmation 		= $notif->urlConfirmation;

			return true;
		}
		return false;
	}

	/**
	 * Teste la présence du fichier de notification. Renvoie le chemin du fichier si présent.
	 *
	 * @access public
	 * @return string : path to notification file or false;
	 */
	public function isNotificationFile() {
		$notiFile = $this->cachePath.'/notification.json';
		if (is_file($notiFile)){
			return $notiFile;
		}
		return false;
	}

	/**
	 * Télécharge le fichier d'export.
	 *
	 * @access public
	 * @return bool True / False
	 */
	public function download() {
		if (!$this->urlRecuperation =='') {
			$url = $this->urlRecuperation;
			$folder = $this->cachePath.'/export.zip';
			if (sbcd_tools::downloadFile($this->urlRecuperation,$folder)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Extrait l'archive de l'export dans le dossier cachePath/export/.
	 *
	 * @access public
	 * @return bool
	 */
	public function extractArchive() {
		$exportFile = $this->cachePath.'/export.zip';
		$exportDest = $this->cachePath.'/export/';
		if (is_file($exportFile)) {
			// Si le dossier de destination n'existe pas, on tente de le créer.
			if (!is_dir($exportDest)) {
				if (!sbcd_tools::safe_mkdir($exportDest)) {
					return false;
				}
			}
			$zip = sbcd_tools::unzip($exportFile,$exportDest);
			return $zip;
		}
		return false;
	}

	/**
	 * Renvoie la confirmation de traitement auprès d'Apidae.
	 *
	 * @access public
	 * @return void
	 */
	public function sendConfirmation() {
		if ($this->urlConfirmation !='' ) {
			$handle = fopen($this->urlConfirmation, "rb");
			if ($contents = stream_get_contents($handle)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Efface le fichier de notification d'export d'Apidae.
	 *
	 * @access public
	 * @return void
	 */
	public function delNotification() {
		if ($notiFile = $this->isNotificationFile()){
			@unlink($notiFile);
			return true;
		}
		return false;
	}

	/**
	 * Efface le fichier d'export téléchargé.
	 *
	 * @access public
	 * @return bool
	 */
	public function delDownloaded() {
		$exportFile = $this->cachePath.'/export.zip';
		if (is_file($exportFile)) {
			@unlink($exportFile);
			return true;
		}
		return false;
	}

	/**
	 * Efface le dossier d'export téléchargé.
	 *
	 * @access public
	 * @return bool
	 */
	public function delExport() {
		$exportDest = $this->cachePath.'/export/';
		if (is_dir($exportDest)) {
			sbcd_tools::safe_rmdir($exportDest);
		}
		return true;
	}

	/**
	 * Actualise les fichiers du silo de données
	 *
	 * @access public
	 * @return bool
	 */
	public function updateSilo() {
		// Adresse de destination du Silo de données
		$exportDest 		= $this->cachePath.'export/';
		$siloDest 			= $this->cachePath.'silo/';
		$siloObjets 		= $siloDest.'objets/';
		$siloObjetsLies		= $siloDest.'objets-lies/';

		// Création des dossiers si absents
		if (!is_dir($siloDest)) {
			if (!sbcd_tools::safe_mkdir($siloDest)) {
				return false;
			}
		}
		if (!is_dir($siloObjets)) {
			if (!sbcd_tools::safe_mkdir($siloObjets)) {
				return false;
			}
		}
		if (!is_dir($siloObjetsLies)) {
			if (!sbcd_tools::safe_mkdir($siloObjetsLies)) {
				return false;
			}
		}

		// Recopie des référentiels
		$referentiels = array(
			'communes.xml', 'criteres_internes.xml', 'elements_reference.xml', 'selections.xml', 'selections.xml', 'territoires.xml',
			'communes.json', 'criteres_internes.json', 'elements_reference.json', 'selections.json', 'selections.json', 'territoires.json'
		);
		foreach($referentiels as $r) {
			if (is_file($exportDest.$r)) {
				copy($exportDest.$r, $siloDest.$r);
			}
		}

		// Recopie des Objets
		if ($files = sbcd_tools::listDir($exportDest.'objets_modifies/')) {
			foreach ($files as $f) {
				$fSilo = str_replace('objets_modifies-', '', $f);
				copy ($exportDest.'/objets_modifies/'.$f,$siloObjets.$fSilo);
			}
		}

		// Recopie des Objets Liés
		if ($files = sbcd_tools::listDir($exportDest.'objets_lies/')) {
			foreach ($files as $f) {
				$fSilo = str_replace('objets_lies_modifies-', '', $f);
				copy ($exportDest.'/objets_lies/'.$f,$siloObjetsLies.$fSilo);
			}
		}

		// Suppression des Objets supprimés
		if (is_file($exportDest.'objets_supprimes.json')) {
			$files2Del = file_get_contents($exportDest.'objets_supprimes.json');
			$files2Del = json_decode($files2Del);		
			foreach ($files2Del as $f) {
				if (is_file($siloObjets.$f.'.json')) {
					unlink($siloObjets.$f.'.json');
				}
			}
		}
		if (is_file($exportDest.'objets_supprimes.xml')) {
			$files2Del = file_get_contents($exportDest.'objets_supprimes.xml');
			$files2Del = simplexml_load_string($files2Del);
			$files2Del = $files2Del->ArrayList;
			foreach ($files2Del->item as $f) {
				if (is_file($siloObjets.$f.'.xml')) {
					unlink($siloObjets.$f.'.xml');
				}
			}
		}

		// Suppression des Objets liés supprimés
		if (is_file($exportDest.'objets_lies_supprimes.json')) {
			$files2Del = file_get_contents($exportDest.'objets_lies_supprimes.json');
			$files2Del = json_decode($files2Del);		
			foreach ($files2Del as $f) {
				if (is_file($siloObjetsLies.$f.'.json')) {
					unlink($siloObjetsLies.$f.'.json');
				}
			}
		}
		if (is_file($exportDest.'objets_lies_supprimes.xml')) {
			$files2Del = file_get_contents($exportDest.'objets_lies_supprimes.xml');
			$files2Del = simplexml_load_string($files2Del);
			$files2Del = $files2Del->ArrayList;
			foreach ($files2Del->item as $f) {
				if (is_file($siloObjetsLies.$f.'.xml')) {
					unlink($siloObjetsLies.$f.'.xml');
				}
			}
		}
		return true;
	}

	/**
	 * Traite les exports si un fichier de notification est présent.
	 *
	 * @access public
	 * @return void
	 */
	public function cronWalk() {
		if ($this->isNotificationFile()) {
			$this->loadNotification();
			$this->download();
			$this->extractArchive();
			$this->updateSilo();
			$this->delDownloaded();
			$this->delExport();
			$this->sendConfirmation();
			$this->delNotification();
			return true;
		}
		return false;
	}
}
?>