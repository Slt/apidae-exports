# Apidae Exports

Ce script permet de créer un ***silo de données*** à partir d'un export mis en place sur [Apidae](https://www.apidae-tourisme.com/).

Il prend en charge les projets au format JSON ou XML indiféremment. En revanche, pour fonctionner correctement, les exports ne doivent pas être configurés pour grouper les objets exportés, ce script se base sur le principe : un objet = une offre.



## Principes de fonctionnement

À la fin de la periode de calcul des sélections et exports, Apidae envoie une notification en POST sur le fichier `notifications.php`.

Ce fichier enregistre les éléments nécessaires au traitement de l'export dans un fichier temporaire `notifications.json` dans le dossier `DATAPATH` défini dans le fichier de configuration. Ce fichier sera supprimé une fois l'export traité.

Le fichier `cron.php`, lancé régulièrement, vérifie l'existence du fichier `notifications.json`. Si le fichier est présent, le traite.



##### Tâches effectuées durant le traitement de l'export :

- Téléchargement de l'archive ZIP mentionnée dans le fichier `notifications.json`.
- Création d'un dossier `export` dans le dossier `DATAPATH`. L'archive téléchargée est extraite dans ce dossier.
- Synchronisation des données entre le dossier `export` et le dossier `silo`.
  - Ajout des nouveaux objets modifiés, si les fichiers étaient déjà présents, ils sont écrasés par leur version mise à jour,
  - Ajout ou mise à jour des nouveaux objets liés modifiés,
  - Suppression des objets et objets liés présents dans les fichiers `objets_supprimes`et  `objets_lies_supprimes`,
  - Suppression du dossier `export` une fois le traitement effectué,
  - Appel de l'URL de confirmation Apidae afin de signaler que l'import est opérationnel,
  - Suppression du fichier `notifications.json`.



## Installation

##### Cloner les fichiers à partir de GitLab

`git clone https://gitlab.com/Slt/apidae-exports.git myFolderName`

##### Configuration

Dans le fichier `config.php`, renseigner la constante `DATAPATH` avec un chemin pointant vers un dossier sur lequel le serveur dispose des droits d'écriture.

Par défaut, la constante `DATAPATH` pointe vers le dossier `/data/`.

##### Activer le CRON

Configurer un appel régulier sur le fichier `cron.php`

##### Configuration technique Apidae

Dans la configuration technique du projet sur Apidae, il faudra faire pointer l'URL de notification vers le fichier `notifications.php`.